package cz.prenek.zonky.rest.client

import cz.prenek.zonky.rest.client.model.Loan
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest
class JsonParserTest extends Specification{

    @Autowired
    JsonParser jsonParser

    def "loans should be parsed correctly"(){

        given: "tested JSON"
        def json = '''
[{"id":403418,"name":"Car","purpose":"1","photos":[{"name":"auto-moto-2-8b26ebda9eff149519862ab44e34e1a4.jpg","url":"/loans/403418/photos/49774"}],"userId":267994,"nickName":"zonky267994","termInMonths":84,"interestRate":0.084900,"revenueRate":0.059900,"annuity":5654.00,"premium":null,"rating":"AA","topped":false,"amount":350000.00,"remainingInvestment":346400.00,"investmentRate":0.010285714285714285,"covered":false,"reservedAmount":12200.00,"datePublished":"2019-02-17T21:36:26.117+01:00","published":true,"deadline":"2019-02-19T21:27:16.555+01:00","myOtherInvestments":null,"borrowerRelatedInvestmentInfo":null,"investmentsCount":14,"questionsCount":0,"region":"11","mainIncomeType":"EMPLOYMENT","questionsAllowed":false,"activeLoansCount":0,"insuranceActive":false,"insuranceHistory":[],"fastcash":true,"multicash":false,"annuityWithInsurance":5654.00},{"id":666, "name":"dummy"}]'''

        when:
        List<Loan> loans = jsonParser.parseLoanJson(json)

        then: "basic properties are set"
        loans
        loans.size() == 2
        loans.each{l ->
            assert( l.getId() != -1)
            assert( l.getName())
            assert( l.getUrl() == null)
        }
    }

    def "empty or null JSON is provided"(){

        when: "Empty JSON"
        def json = ""
        jsonParser.parseLoanJson(json)

        then:
        IllegalArgumentException ex = thrown()
        ex.message == 'Empty JSON string for parsing'

        when: "Null JSON"
        json = null
        jsonParser.parseLoanJson(json)

        then:
        ex = thrown()
        ex.message == 'Empty JSON string for parsing'

    }
}
