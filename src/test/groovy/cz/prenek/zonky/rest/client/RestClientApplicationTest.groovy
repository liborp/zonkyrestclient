package cz.prenek.zonky.rest.client

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpEntity
import org.springframework.web.client.RestTemplate
import spock.lang.Specification

@SpringBootTest
class RestClientApplicationTest extends Specification{

    @Autowired
    JsonParser jsonParser

    @Autowired
    ZonkyLoanReaderTask loanReaderTask

    @Autowired
    LoanWriter loanWriter

    @Autowired
    RestTemplate restTemplate

    @Autowired
    HttpEntity httpEntity

    def "beans should be loaded in context"(){
        expect: "beans loaded"
        jsonParser
        loanWriter
        loanReaderTask
        restTemplate
        httpEntity
    }
}
