package cz.prenek.zonky.rest.client;

import cz.prenek.zonky.rest.client.model.Loan;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class LoanWriter {

    public void write(List<Loan> loans) {
        loans.forEach(System.out::println);

    }
}
