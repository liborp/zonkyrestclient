package cz.prenek.zonky.rest.client;

import cz.prenek.zonky.rest.client.model.Loan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;

@Component
public class ZonkyLoanReaderTask {

    private JsonParser jsonParser;
    private LoanWriter loanWriter;

    private RestTemplate restTemplate;
    private HttpEntity headersKeeper;

    @Value("${cz.prenek.url.api.zonky}")
    private String zonkyApiUrl;

    private static final Logger logger = LoggerFactory.getLogger(Scheduler.class);

    public ZonkyLoanReaderTask(JsonParser jsonParser, LoanWriter loanWriter, RestTemplate restTemplate, HttpEntity headersKeeper) {
        this.jsonParser = jsonParser;
        this.loanWriter = loanWriter;
        this.restTemplate = restTemplate;
        this.headersKeeper = headersKeeper;
    }

    public void read() throws IOException {

        ResponseEntity<String> response = restTemplate.exchange(zonkyApiUrl, HttpMethod.GET, headersKeeper, String.class);

        if (logger.isDebugEnabled()) {
            logger.debug(response.getBody());
        }

        List<Loan> loans = jsonParser.parseLoanJson(response.getBody());
        loanWriter.write(loans);
    }
}
