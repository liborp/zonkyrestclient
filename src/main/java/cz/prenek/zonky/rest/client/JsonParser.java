package cz.prenek.zonky.rest.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import cz.prenek.zonky.rest.client.model.Loan;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class JsonParser {

    private ObjectMapper objectMapper;

    public JsonParser() {
        objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new StdDateFormat());
    }

    public List<Loan> parseLoanJson(String json) throws IOException {
        if (json == null || json.isEmpty()) {
            throw new IllegalArgumentException("Empty JSON string for parsing");
        }

        TypeFactory typeFactory = objectMapper.getTypeFactory();

        return objectMapper.readValue(json, typeFactory.constructCollectionType(List.class, Loan.class));
    }
}
