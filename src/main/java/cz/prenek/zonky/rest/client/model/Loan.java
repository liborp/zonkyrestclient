package cz.prenek.zonky.rest.client.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Loan {

    private int id;
    private String name;
    private String url;
    private String nickName;
    private int termInMonths;
    private int remainingInvestment;
    private int reservedAmount;
    private int userId;
    private float interestRate;
    private float investmentRate;
    private Date datePublished;

    public Date getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(Date datePublished) {
        this.datePublished = datePublished;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTermInMonths() {
        return termInMonths;
    }

    public void setTermInMonths(int termInMonths) {
        this.termInMonths = termInMonths;
    }

    public int getRemainingInvestment() {
        return remainingInvestment;
    }

    public void setRemainingInvestment(int remainingInvestment) {
        this.remainingInvestment = remainingInvestment;
    }

    public int getReservedAmount() {
        return reservedAmount;
    }

    public void setReservedAmount(int reservedAmount) {
        this.reservedAmount = reservedAmount;
    }

    public float getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(float interestRate) {
        this.interestRate = interestRate;
    }

    public float getInvestmentRate() {
        return investmentRate;
    }

    public void setInvestmentRate(float investmentRate) {
        this.investmentRate = investmentRate;
    }

    @Override
    public String toString() {
        return "Loan{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", nickName='" + nickName + '\'' +
                ", termInMonths=" + termInMonths +
                ", remainingInvestment=" + remainingInvestment +
                ", reservedAmount=" + reservedAmount +
                ", interestRate=" + interestRate * 100 + "%" +
                ", investmentRate=" + investmentRate +
                ", datePublished=" + datePublished +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Loan loan = (Loan) o;
        return id == loan.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
